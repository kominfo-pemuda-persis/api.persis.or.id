# Persis Backend

## What you need to know before contributing

- This project using [Badaso](https://github.com/uasoft-indonesia/badaso)
- This project use [laravel code style](https://laravel.com/docs/10.x/contributions#coding-style).

## How to run for development

### via local machine

- composer install
- php artisan migrate
- composer dump-autoload
- php artisan db:seed
- php artisan storage:link
- yarn
- yarn dev
- php artisan serve

### via Docker

- Make sure docker installed on your device.
- Run `docker compose build`
- Run `docker compose up -d`
- Run `docker compose exec laravel.test composer install`
- Run `docker compose exec laravel.test php artisan migrate`
- Run `docker compose exec laravel.test composer dump-autoload`
- Run `docker compose exec laravel.test php artisan db:seed`
- Run `docker compose exec laravel.test php artisan storage:link`
- Run `docker compose exec laravel.test yarn`
- Run `docker compose exec laravel.test yarn dev`
