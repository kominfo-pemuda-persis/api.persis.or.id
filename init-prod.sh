#!/bin/bash

echo \#\!\/bin\/bash > init.sh

echo export ANALYTICS_VIEW_ID=$ANALYTICS_VIEW_ID >>init.sh
echo export APP_ENV=$APP_ENV >>init.sh
echo export APP_DEBUG=$APP_DEBUG >>init.sh
echo export APP_NAME=$APP_NAME >>init.sh
echo export APP_URL=$APP_URL >>init.sh
echo export ARCANEDEV_LOGVIEWER_MIDDLEWARE=$ARCANEDEV_LOGVIEWER_MIDDLEWARE >>init.sh
echo export APP_KEY=$APP_KEY >>init.sh


echo export AWS_BUCKET=$AWS_BUCKET >>init.sh
echo export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION >>init.sh
echo export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY >>init.sh
echo export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID >>init.sh
echo export AWS_URL=$AWS_URL >>init.sh
echo export AWS_USE_PATH_STYLE_ENDPOINT=$AWS_USE_PATH_STYLE_ENDPOINT >>init.sh

echo export BACKUP_DISK=$BACKUP_DISK >>init.sh
echo export BACKUP_TARGET=$BACKUP_TARGET >>init.sh
echo export BADASO_AUTH_TOKEN_LIFETIME=$BADASO_AUTH_TOKEN_LIFETIME >>init.sh
echo export BADASO_LICENSE_KEY=$BADASO_LICENSE_KEY >>init.sh
echo export BADASO_SWAGGER_HOST=$BADASO_SWAGGER_HOST >>init.sh

echo export CONTAINER_APP_NAME=$CONTAINER_APP_NAME >>init.sh

echo export DB_CONNECTION=$DB_CONNECTION >>init.sh
echo export DB_DATABASE=$DB_DATABASE >>init.sh
echo export DB_HOST=$DB_HOST >>init.sh
echo export DB_PASSWORD=$DB_PASSWORD >>init.sh
echo export DB_PORT=$DB_PORT >>init.sh
echo export DB_USERNAME=$DB_USERNAME >>init.sh

echo export DROPBOX_AUTH_TOKEN=$DROPBOX_AUTH_TOKEN >>init.sh
echo export FILESYSTEM_DRIVER=$FILESYSTEM_DRIVER >>init.sh

echo export GOOGLE_DRIVE_CLIENT_ID=$GOOGLE_DRIVE_CLIENT_ID >>init.sh
echo export GOOGLE_DRIVE_CLIENT_SECRET=$GOOGLE_DRIVE_CLIENT_SECRET >>init.sh
echo export GOOGLE_DRIVE_FOLDER_ID=$GOOGLE_DRIVE_FOLDER_ID >>init.sh
echo export GOOGLE_DRIVE_REFRESH_TOKEN=$GOOGLE_DRIVE_REFRESH_TOKEN >>init.sh

echo export JWT_SECRET=$JWT_SECRET >>init.sh

echo export LOG_CHANNEL=$LOG_CHANNEL >>init.sh
echo export LOG_LEVEL=$LOG_LEVEL >>init.sh

echo export BROADCAST_DRIVER=$BROADCAST_DRIVER >>init.sh
echo export CACHE_DRIVER=$CACHE_DRIVER >>init.sh
echo export SESSION_DRIVER=$SESSION_DRIVER >>init.sh
echo export SESSION_LIFETIME=$SESSION_LIFETIME >>init.sh
echo export MEMCACHED_HOST=$MEMCACHED_HOST >>init.sh

echo export MIX_ADMIN_PANEL_ROUTE_PREFIX=$MIX_ADMIN_PANEL_ROUTE_PREFIX >>init.sh
echo export MIX_ANALYTICS_ACCOUNT_ID=$MIX_ANALYTICS_ACCOUNT_ID >>init.sh
echo export MIX_ANALYTICS_TRACKING_ID=$MIX_ANALYTICS_TRACKING_ID >>init.sh
echo export MIX_ANALYTICS_VIEW_ID=$MIX_ANALYTICS_VIEW_ID >>init.sh
echo export MIX_ANALYTICS_WEBPROPERTY_ID=$MIX_ANALYTICS_WEBPROPERTY_ID >>init.sh
echo export MIX_API_DOCUMENTATION_ANNOTATION_ROUTE=$MIX_API_DOCUMENTATION_ANNOTATION_ROUTE >>init.sh
echo export MIX_API_DOCUMENTATION_ROUTE=$MIX_API_DOCUMENTATION_ROUTE >>init.sh
echo export MIX_API_ROUTE_PREFIX=$MIX_API_ROUTE_PREFIX >>init.sh
echo export MIX_BADASO_MENU=$MIX_BADASO_MENU >>init.sh
echo export MIX_BADASO_MODULES=$MIX_BADASO_MODULES >>init.sh
echo export MIX_BLOG_POST_URL_PREFIX=$MIX_BLOG_POST_URL_PREFIX >>init.sh
echo export MIX_DATE_FORMAT=$MIX_DATE_FORMAT >>init.sh
echo export MIX_DATETIME_FORMAT=$MIX_DATETIME_FORMAT >>init.sh
echo export MIX_DEFAULT_MENU=$MIX_DEFAULT_MENU >>init.sh
echo export MIX_FIREBASE_API_KEY=$MIX_FIREBASE_API_KEY >>init.sh
echo export MIX_FIREBASE_APP_ID=$MIX_FIREBASE_APP_ID >>init.sh
echo export MIX_FIREBASE_AUTH_DOMAIN=$MIX_FIREBASE_AUTH_DOMAIN >>init.sh
echo export MIX_FIREBASE_MEASUREMENT_ID=$MIX_FIREBASE_MEASUREMENT_ID >>init.sh
echo export MIX_FIREBASE_MESSAGE_SEENDER=$MIX_FIREBASE_MESSAGE_SEENDER >>init.sh
echo export MIX_FIREBASE_PROJECT_ID=$MIX_FIREBASE_PROJECT_ID >>init.sh
echo export MIX_FIREBASE_SERVER_KEY=$MIX_FIREBASE_SERVER_KEY >>init.sh
echo export MIX_FIREBASE_STORAGE_BUCKET=$MIX_FIREBASE_STORAGE_BUCKET >>init.sh
echo export MIX_FIREBASE_WEB_PUSH_CERTIFICATES=$MIX_FIREBASE_WEB_PUSH_CERTIFICATES >>init.sh
echo export MIX_LOG_VIEWER_ROUTE=$MIX_LOG_VIEWER_ROUTE >>init.sh
echo export MIX_PUSHER_APP_CLUSTER=$MIX_PUSHER_APP_CLUSTER >>init.sh
echo export MIX_PUSHER_APP_KEY=$MIX_PUSHER_APP_KEY >>init.sh
echo export MIX_TIME_FORMAT=$MIX_TIME_FORMAT >>init.sh

echo export PUSHER_APP_CLUSTER=$PUSHER_APP_CLUSTER >>init.sh
echo export PUSHER_APP_ID=$PUSHER_APP_ID >>init.sh
echo export PUSHER_APP_KEY=$PUSHER_APP_KEY >>init.sh
echo export PUSHER_APP_SECRET=$PUSHER_APP_SECRET >>init.sh

echo export QUEUE_CONNECTION=$QUEUE_CONNECTION >>init.sh

echo export REDIS_HOST=$REDIS_HOST >>init.sh
echo export REDIS_PASSWORD=$REDIS_PASSWORD >>init.sh

echo export MAIL_ENCRYPTION=$MAIL_ENCRYPTION >>init.sh
echo export MAIL_PASSWORD=$MAIL_PASSWORD >>init.sh
echo export MAIL_USERNAME=$MAIL_USERNAME >>init.sh
echo export MAIL_PORT=$MAIL_PORT >>init.sh
echo export MAIL_HOST=$MAIL_HOST >>init.sh
echo export MAIL_DRIVER=$MAIL_DRIVER >>init.sh
echo export MAIL_FROM_ADDRESS=$MAIL_FROM_ADDRESS >>init.sh
echo export MAIL_FROM_NAME=$MAIL_FROM_NAME >>init.sh

echo export SENTRY_LARAVEL_DSN=$SENTRY_LARAVEL_DSN >>init.sh

echo export PORT_MAPPING=$PORT_MAPPING >>init.sh
echo export TAG_API_PERSIS=$CI_COMMIT_REF_NAME >>init.sh
echo export REPOSITORY_ECR_URL=$REPOSITORY_ECR_URL >>init.sh

echo "aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin $ECR_URL" >>init.sh
echo docker-compose down >>init.sh
echo docker system prune -af >>init.sh
echo docker pull $REPOSITORY_ECR_URL:$CI_COMMIT_REF_NAME >>init.sh
echo docker-compose up -d >>init.sh

