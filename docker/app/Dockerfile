FROM alpine:3.19

ARG ENV_BUILD
ENV ENV_BUILD_VAR=$ENV_BUILD

# Install packages
RUN apk --no-cache add ca-certificates php82 php82-fpm \
  php82-opcache php82-openssl php82-curl \
  nginx supervisor curl php82-json php82-phar php82-iconv \
  php-exif php-sodium php82-pdo php82-mbstring php82-dom \
  php82-zip php-mysqli php-sqlite3 php-session php-bcmath \
  php82-common php-gd php-intl php82-fileinfo php82-pdo_mysql \
  php82-tokenizer php82-xml php82-xmlwriter php82-simplexml \
  php-ctype php82-xmlreader


RUN echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/main/" >> /etc/apk/repositories \
  && apk add --update nodejs=12.22.12-r0 npm=12.22.12-r0

# install composer
RUN curl -s https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Configure nginx
COPY docker/config/nginx.conf /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY docker/config/fpm-pool.conf /etc/php82/php-fpm.d/www.conf
COPY docker/config/php.ini /etc/php82/persis.ini

# Configure supervisord
COPY docker/config/supervisord.conf /etc/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/api-persis

# Add application
WORKDIR /var/www/api-persis
COPY --chown=nobody . /var/www/api-persis/

RUN composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts && \
  chmod -R 755 /var/www/api-persis/storage && \
  chmod -R 755 /var/www/api-persis/public && \
  composer dump-autoload

RUN echo "$ENV_BUILD_VAR" > .env
RUN chown -R nobody.nobody .env

RUN php artisan key:generate && \
  php artisan config:clear && \
  php artisan cache:clear && \
  php artisan view:clear && \
  php artisan route:clear && \
  php artisan badaso:setup

RUN npm install && npm run prod

RUN chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/log/php82 && \
  chown -R nobody.nobody /var/www/api-persis/public && \
  chown -R nobody.nobody /var/log/nginx

# Switch to use a non-root user from here on
USER nobody
# Expose the port nginx is reachable on
EXPOSE 8080
EXPOSE 443

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping
