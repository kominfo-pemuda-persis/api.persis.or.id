<?php

namespace App\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Force SSL or HTTPS on production mode or testing mode
        if (env('APP_ENV') == 'production' || env('APP_ENV') == 'testing') {
            URL::forceScheme('https');
        }
    }
}
