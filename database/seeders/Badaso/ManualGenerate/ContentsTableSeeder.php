<?php

namespace Database\Seeders\Badaso\ManualGenerate;

use Illuminate\Database\Seeder;
use Uasoft\Badaso\Facades\Badaso;
use Uasoft\Badaso\Module\Content\Models\Content;

class ContentsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        \DB::beginTransaction();
        try {
            Content::query()->delete();

            Content::insert(array(
                0 =>
                array(
                    'id' => 1,
                    'slug' => 'config',
                    'label' => 'Config',
                    'value' => '{"framework":{"name":"framework","label":"Framework","type":"group","data":{"companyLogo":{"name":"companyLogo","label":"Company Logo","type":"image","data":null},"companyFavicon":{"name":"companyFavicon","label":"Company Favicon","type":"text","data":"favicon.ico"}}},"profile":{"name":"profile","label":"Profile","type":"group","data":{"contactPhone":{"name":"contactPhone","label":"Contact Phone","type":"text","data":"(022) 4220705"},"contactEmail":{"name":"contactEmail","label":"Contact Email","type":"text","data":"info@persis.or.id"},"contactUsEmail":{"name":"contactUsEmail","label":"Contact Us Email Notification","type":"text","data":"info@persis.or.id"},"companyName":{"name":"companyName","label":"Company Name","type":"text","data":"Persatuan Islam"},"companyAddress":{"name":"companyAddress","label":"Company Address Office","type":"text","data":"Jl. Perintis Kemerdekaan No.2, Babakan Ciamis, Sumur Bandung, Kota Bandung, Jawa Barat 40117"},"contactFacebook":{"name":"contactFacebook","label":"Facebook","type":"text","data":null},"contactTwitter":{"name":"contactTwitter","label":"Twitter","type":"text","data":null},"contactGplus":{"name":"contactGplus","label":"Google+","type":"text","data":null},"contactInstagram":{"name":"contactInstagram","label":"Instagram","type":"text","data":null},"contactLinkedin":{"name":"contactLinkedin","label":"Linkedin","type":"text","data":null},"contactYoutube":{"name":"contactYoutube","label":"Youtube Channel","type":"text","data":null}}},"web":{"name":"web","label":"Web","type":"group","data":{"sitename":{"name":"sitename","label":"Website Name","type":"text","data":"Persatuan Islam"},"metaKeyword":{"name":"metaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"metaDesc":{"name":"metaDesc","label":"SEO Meta Description","type":"text","data":null},"metaTitle":{"name":"metaTitle","label":"SEO Meta Title","type":"text","data":"Persatuan Islam | Website Official PP Persatuan Islam"},"googleAnalytics":{"name":"googleAnalytics","label":"Kode Google Analytics","type":"text","data":null},"siteurl":{"name":"siteurl","label":"Website URL","type":"text","data":"www.persis.or.id"},"facebookAppId":{"name":"facebookAppId","label":"Facebook App ID","type":"text","data":"548145835577347"},"sharethisPropertyId":{"name":"sharethisPropertyId","label":"ShareThis Property ID","type":"text","data":"59e0325fadad880012f72539"},"facebookAppSecret":{"name":"facebookAppSecret","label":"Facebook App Secret","type":"text","data":"5166dbd2d86ff127999d0bf0d1c8695e"},"livestreamVideoUrl":{"name":"livestreamVideoUrl","label":"Live Streaming URL","type":"text","data":"https:\\/\\/www.youtube.com\\/embed\\/VXW5aX13WNg"}}}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                1 =>
                array(
                    'id' => 2,
                    'slug' => 'tentang-persis',
                    'label' => 'Tentang Persis.or.id',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Tentang Persis.or.id"},"contents":{"name":"contents","label":"Contents","type":"text","data":"<p>Media persis.or.id merupakan website official Pimpinan Pusat Persatuan Islam (PP Persis). Media ini dibawah bidgar Kominfo dan HMK PP Persis. Informasi seputar jamiyyah Persatuan Islam, kajian-kajian keislaman, hingga kepesantrenan senantiasa di update setiap harinya.<\\/p>  <p><b>Tasykil persis.or.id masa jihad 2016-2020<\\/b><\\/p>   <b>Penanggungjawab:<\\/b> KH. Uus Muhammad Ruhiyat, M.Pd (Ketua HMK PP Persis) Jejen&nbsp; Zaenudin, M.Pd (Ketua Kominfo PP Persis) <br\\/>  <b>Pimpinan Redaksi:<\\/b> Muhammad Yahya Firdaus  <b>Editor\\/Publisher:<\\/b> Henry Lukmanul Hakim Taufik Ginanjar  <b>Website Development:<\\/b> Muhammad Rijal  <b>Desain Grafis\\/Ilustrator:<\\/b> Ipan Sopian  <b>Marketing &amp; Kerjasama:<\\/b> Muhammad Yasin &nbsp;<\\/p>"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":"Tentang Persis.or.id"},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":"website official Pimpinan Pusat Persatuan Islam (PP Persis)"},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                2 =>
                array(
                    'id' => 3,
                    'slug' => 'kebijakan-ketentuan',
                    'label' => 'Kebijakan & Ketentuan',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Kebijakan & Ketentuan"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Bebaskan saja lah"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                3 =>
                array(
                    'id' => 4,
                    'slug' => 'marketing-beriklan',
                    'label' => 'Marketing & Beriklan',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Marketing & Beriklan"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Di sini anda beriklan dan lain2"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                4 =>
                array(
                    'id' => 5,
                    'slug' => 'kontak',
                    'label' => 'Hubungi Kami',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Hubungi Kami"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Silahkan hubungi kami di:"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                5 =>
                array(
                    'id' => 6,
                    'slug' => 'karya-imtaq',
                    'label' => 'Karya Imtaq',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Karya Imtaq"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Ini tentang karya imtaq"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                6 =>
                array(
                    'id' => 7,
                    'slug' => 'pusat-zakat-umat',
                    'label' => 'Pusat Zakat Umat',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Pusat Zakat Umat"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Tentang zakat umat"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                7 =>
                array(
                    'id' => 8,
                    'slug' => 'majalah-risalah',
                    'label' => 'Majalah Risalah',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Majalah Risalah"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Segalanya tentang majalah"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                8 =>
                array(
                    'id' => 9,
                    'slug' => 'photography',
                    'label' => 'Persis Photography',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Persis Photography"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Persis fotografi yang berkaitan dll"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                9 =>
                array(
                    'id' => 10,
                    'slug' => 'dakwart',
                    'label' => 'Persis Dakwart',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Persis Dakwart"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Dakwah umat dan lainnya"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                10 =>
                array(
                    'id' => 11,
                    'slug' => 'sejarah',
                    'label' => 'Sejarah Persatuan Islam',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Sejarah Persatuan Islam"},"contents":{"name":"contents","label":"Contents","type":"text","data":"<p>Persatuan Islam (disingkat Persis) adalah sebuah organisasi Islam di Indonesia. Persis didirikan pada 12 September 1923 di Bandung oleh sekelompok Islam yang berminat dalam pendidikan dan aktivitas keagamaan yang dipimpin oleh Haji Zamzam dan Haji Muhammad Yunus.<\\/p><p>Persis didirikan dengan tujuan untuk memberikan pemahaman Islam yang sesuai dengan aslinya yang dibawa oleh Rasulullah Saw dan memberikan pandangan berbeda dari pemahaman Islam tradisional yang dianggap sudah tidak orisinil karena bercampur dengan budaya lokal, sikap taklid buta, sikap tidak kritis, dan tidak mau menggali Islam lebih dalam dengan membuka Kitab-kitab Hadits yang shahih. Oleh karena itu, lewat para ulamanya seperti Ahmad Hassan yang juga dikenal dengan Hassan Bandung atau Hassan Bangil, Persis mengenalkan Islam yang hanya bersumber dari Al-Quran dan Hadits (sabda Nabi). Organisasi Persatuan Islam telah tersebar di banyak provinsi antara lain Jawa Barat, Jawa Timur, DKI Jakarta, Banten, Lampung, Bengkulu, Riau, Jambi, Gorontalo, dan masih banyak provinsi lain yang sedang dalam proses perintisan. Persis bukan organisasi keagamaan yang berorientasi politik namun lebih fokus terhadap Pendidikan Islam dan Dakwah dan berusaha menegakkan ajaran Islam secara utuh tanpa dicampuri khurafat, syirik, dan bid\\u2019ah yang telah banyak menyebar di kalangan awwam orang Islam.<\\/p><p><br><\\/p><p><b>Jam\\u2019iyyah Persis berasaskan Islam<\\/b><\\/p><p>Jam\\u2019iyyah Persis bertujuan terlaksananya syari\\u2019at Islam berlandaskan al-Quran dan as-Sunnah secara kaffah dalam segala aspek kehidupan.<\\/p><p><br><\\/p><p><b>Kelembagaan<\\/b><\\/p><ul><li>Pimpinan Wilayah. Pimpinan Wilayah yang telah didirikan oleh jam\\u2019iyyah terdiri dari 16 PW.<\\/li><li>Pimpinan Daerah. Dari 16 Pimpinan Wilayah membawahi jalur jam\\u2019iyyah sebanyak 62 Pimpinan Daerah.<\\/li><li>Pimpinan Cabang. Dari 62 Pimpinan Daerah membawahi 358 Pimpinan Cabang<\\/li><\\/ul><p><br><\\/p><p><b>Lembaga Pendidikan<\\/b><\\/p><p>Persatuan Islam yang gerakan utamanya adalah pendidikan telah menyiapkan lembaga-lembaga pendidikan berbasis kepesantrenan sebanyak 230 pesantren.<\\/p><p><br><\\/p><p><b>Tokoh<\\/b><\\/p><ul><li>Muhammad Isa Anshary, politikus dan pejuang Indonesia.<\\/li><li>Mohammad Natsir, mantan Perdana Menteri Indonesia<\\/li><li>Ahmad Hassan, teman debat Soekarno ketika di Bandung<\\/li><li>Haji Zamzam, pendiri Persis<\\/li><li>H. Eman Sar\\u2019an<\\/li><li>Achyar Syuhada, ulama terkemuka Persis<\\/li><li>Mohammad Yunus, ulama Persis<\\/li><li>K.H. E. Abdurrahman, pemimpin Persis tahun 1962-1983<\\/li><li>K.H&nbsp; A. Latif Muchtar<\\/li><li>K.H. Shiddiq Amien, Mba Mantan Ketua Umum persis<\\/li><li>K.H. Ikin Shadikin, ulama terkemuka Persis<\\/li><li>K.H. Usman Sholehudin, Ketua Dewan Hisbath<\\/li><li>K.H. Aceng Zakaria<\\/li><li>K.H. M. Romli<\\/li><li>K.H. Entang Muchtar ZA<\\/li><\\/ul>"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":"Sejarah Persatuan Islam"},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                11 =>
                array(
                    'id' => 12,
                    'slug' => 'tasykil',
                    'label' => 'Tasykil',
                    'value' => '{"title":{"name":"title","label":"Title","type":"text","data":"Tasykil"},"contents":{"name":"contents","label":"Contents","type":"text","data":"Tasykil persis apa aja"},"seoMetaTitle":{"name":"seoMetaTitle","label":"SEO Meta Title","type":"text","data":null},"seoMetaDesc":{"name":"seoMetaDesc","label":"SEO Meta Desc","type":"text","data":null},"seoMetaKeyword":{"name":"seoMetaKeyword","label":"SEO Meta Keyword","type":"text","data":null},"published":{"name":"published","label":"Published","type":"text","data":"Y"}}',
                    'created_at' => '2017-10-11 22:50:27',
                    'updated_at' => '2017-10-11 22:50:27',
                ),
                12 =>
                array(
                    'id' => 13,
                    'slug' => 'live-streaming',
                    'label' => 'Live Streaming',
                    'value' => '{ "liveStreamingUrl": { "name": "liveStreamingUrl", "label": "Live Streaming URL", "type": "text", "data": "https:\/\/www.youtube.com\/embed\/VXW5aX13WNg" }, "persisTv": { "name": "persisTv", "label": "Persis TV", "type": "group", "data": { "facebookPage": { "name": "facebookPage", "label": "Facebook Page", "type": "group", "data": { "icon": { "name": "icon", "label": "Icon", "type": "text", "data": "fab fa-facebook-f" }, "url": { "name": "url", "label": "URL", "type": "url", "data": { "url": "https:\/\/www.facebook.com\/persistvchannel", "text": "Persis TV" } } } }, "instagramPage": { "name": "instagramPage", "label": "Instagram Page", "type": "group", "data": { "icon": { "name": "icon", "label": "Icon", "type": "text", "data": "fab fa-instagram" }, "url": { "name": "url", "label": "URL", "type": "url", "data": { "url": "https:\/\/www.instagram.com\/persistvchannel", "text": "Persis TV" } } } }, "youtubePage": { "name": "youtubePage", "label": "Youtube Page", "type": "group", "data": { "icon": { "name": "icon", "label": "Icon", "type": "text", "data": "fab fa-youtube" }, "url": { "name": "url", "label": "URL", "type": "url", "data": { "url": "https:\/\/www.youtube.com\/channel\/UCg1PjFz0AsjUacJTB4gI2qA", "text": "Persis TV" } } } } } }, "persisPhotography": { "name": "persisPhotography", "label": "Persis Photography", "type": "group", "data": { "facebookPage": { "name": "facebookPage", "label": "Facebook Page", "type": "group", "data": { "icon": { "name": "icon", "label": "Icon", "type": "text", "data": "fab fa-facebook-f" }, "url": { "name": "url", "label": "URL", "type": "url", "data": { "url": "https:\/\/www.facebook.com\/persisphotography", "text": "Persis Photography" } } } }, "instagramPage": { "name": "instagramPage", "label": "Instagram Page", "type": "group", "data": { "icon": { "name": "icon", "label": "Icon", "type": "text", "data": "fab fa-instagram" }, "url": { "name": "url", "label": "URL", "type": "url", "data": { "url": "https:\/\/www.instagram.com\/persisphotography", "text": "Persis Photography" } } } }, "youtubePage": { "name": "youtubePage", "label": "Youtube Page", "type": "group", "data": { "icon": { "name": "icon", "label": "Icon", "type": "text", "data": "fab fa-youtube" }, "url": { "name": "url", "label": "URL", "type": "url", "data": { "url": "https:\/\/youtube.com\/c\/persisphotography?sub_confirmation=1", "text": "Persis Photography" } } } } } }, "seoMetaTitle": { "name": "seoMetaTitle", "label": "SEO Meta Title", "type": "text", "data": "Live Streaming at Persatuan Islam" }, "seoMetaDesc": { "name": "seoMetaDesc", "label": "SEO Meta Desc", "type": "text", "data": "video dakwah, live streaming pengajian" }, "seoMetaKeyword": { "name": "seoMetaKeyword", "label": "SEO Meta Keyword", "type": "text", "data": "live streaming pengajian" } }',
                    'created_at' => '2018-04-16 09:09:46',
                    'updated_at' => '2018-04-16 09:09:46',
                ),
                13 =>
                array(
                    'id' => 14,
                    'slug' => 'header',
                    'label' => 'header',
                    'value' => '{"office":{"name":"office","label":"Office","type":"group","data":{"address-1":{"name":"address-1","label":"Address 1","type":"text","data":"Jl. Cik Ditiro No.23 Yogyakarta"},"address-2":{"name":"address-2","label":"Address 2","type":"text","data":"Jl. Menteng Raya No. 62 Jakarta Pusat"}}},"logo":{"name":"logo","label":"Logo","type":"image","data":"https://web-persis.s3.ap-southeast-1.amazonaws.com/files/shares/PERSIS_NOBG.png"}}',
                    'created_at' => '2021-06-15 07:01:21',
                    'updated_at' => '2021-06-15 07:12:37',
                ),
                14 =>
                array(
                    'id' => 15,
                    'slug' => 'social-media',
                    'label' => 'social-media',
                    'value' => '{"facebook-logo":{"name":"facebook-logo","label":"Facebook Logo","type":"image","data":null},"facebook-link":{"name":"facebook-link","label":"Facebook Link","type":"url","data":{"url":"https:\\/\\/www.facebook.com\\/PeryarikatanMuhammadiyah\\/","text":"Facebook"}},"twitter-logo":{"name":"twitter-logo","label":"Twitter Logo","type":"image","data":null},"twitter-link":{"name":"twitter-link","label":"Twitter Link","type":"url","data":{"url":"https:\\/\\/twitter.com\\/muhammadiyah","text":"Twitter"}},"instagram-logo":{"name":"instagram-logo","label":"Instagram Logo","type":"image","data":null},"instagram-link":{"name":"instagram-link","label":"Instagram Link","type":"url","data":{"url":"https:\\/\\/www.instagram.com\\/lensamu\\/","text":"Instagram"}},"youtube-logo":{"name":"youtube-logo","label":"Youtube Logo","type":"image","data":null},"youtube-link":{"name":"youtube-link","label":"Youtube Link","type":"url","data":{"url":"https:\\/\\/www.instagram.com\\/lensamu\\/","text":"Youtube"}}}',
                    'created_at' => '2021-06-15 07:10:25',
                    'updated_at' => '2021-06-15 07:45:59',
                ),
                15 =>
                array(
                    'id' => 16,
                    'slug' => 'footer',
                    'label' => 'footer',
                    'value' => '{"copyright":{"name":"copyright","label":"Copyright","type":"group","data":{"year":{"name":"year","label":"Year","type":"text","data":"2020"},"central-leadership":{"name":"central-leadership","label":"Central Leadership","type":"url","data":{"url":"https:\\/\\/muhammadiyah.or.id\\/muhammadiyah.or.id","text":"Pimpinan Pusat Muhammadiyah"}},"motto":{"name":"motto","label":"Motto","type":"text","data":"Cahaya Islam Berkemajuan."}}}}',
                    'created_at' => '2021-06-15 08:03:48',
                    'updated_at' => '2021-06-15 08:05:14',
                ),
                16 =>
                array(
                    'id' => 17,
                    'slug' => 'ikhwan',
                    'label' => 'ikhwan',
                    'value' => '{"jabar":{"name":"jabar","label":"Persis Jabar","type":"group","data":{"image":{"name":"image","label":"Image","type":"image","data":"https://www.nu.or.id/assets/images/jabar.png"},"url":{"name":"url","label":"URL","type":"url","data":{"url":"https://persis.or.id/","text":"Jabar"}}}},"jatim":{"name":"jatim","label":"Persis Jatim","type":"group","data":{"image":{"name":"image","label":"Image","type":"image","data":"https://www.nu.or.id/assets/images/jatim.png"},"url":{"name":"url","label":"URL","type":"url","data":{"url":"https://persis.or.id/","text":"Jatim"}}}},"jateng":{"name":"jateng","label":"Persis Jateng","type":"group","data":{"image":{"name":"image","label":"Image","type":"image","data":"https://www.nu.or.id/assets/images/jateng.png"},"url":{"name":"url","label":"URL","type":"url","data":{"url":"https://persis.or.id/","text":"Jateng"}}}},"banten":{"name":"banten","label":"Persis Banten","type":"group","data":{"image":{"name":"image","label":"Image","type":"image","data":"https://www.nu.or.id/assets/images/banten.png"},"url":{"name":"url","label":"URL","type":"url","data":{"url":"https://persis.or.id/","text":"Banten"}}}}}',
                    'created_at' => '2021-06-23 09:47:54',
                    'updated_at' => '2021-06-23 10:15:15',
                ),
                17 =>
                array(
                    'id' => 18,
                    'slug' => 'popup',
                    'label' => 'popup',
                    'value' => '{"popup":{"name":"popup","label":"Popup","type":"image","data":"https://www.siagaonline.com/foto_berita/22IMG-20210308-WA0055.jpg"}, "url":{"name":"url","label":"Banner URL","type":"text","data":"https://google.com"}}',
                    'created_at' => '2021-06-23 09:53:14',
                    'updated_at' => '2021-06-23 09:53:25',
                ),
                18 =>
                array(
                    'id' => 19,
                    'slug' => 'navbar-banner',
                    'label' => 'Navbar Banner',
                    'value' => '{"image":{"name":"image","label":"Banner Image","type":"image","data":"https://1.bp.blogspot.com/-YYVc5vrpXNQ/YAmHR2IThuI/AAAAAAAAGIk/6ks1vHnCCc0ng9-70mm4VV4tzgw9BnBAwCNcBGAsYHQ/s16000/donasi%2Bad1.jpg"},"url":{"name":"url","label":"Banner URL","type":"text","data":"https://kitasaudara.org/program/muktamar_xvi_persatuan_islam"}}',
                    'created_at' => '2021-06-23 09:53:14',
                    'updated_at' => '2021-06-23 09:53:25',
                ),
                19 =>
                array(
                    'id' => 20,
                    'slug' => 'sidebar-banner',
                    'label' => 'Sidebar Banner',
                    'value' => '{"image":{"name":"image","label":"Banner Image","type":"image","data":"https://1.bp.blogspot.com/-6IRdzJEfifQ/YVfVbKFkGZI/AAAAAAAAC1U/zS1AISFXTzgSJ8quhJpv0AmV5FjTnQ9PQCNcBGAsYHQ/s16000/Visual%2BB%2B%252824x15%2Bcm%2529.jpg"},"url":{"name":"url","label":"Banner URL","type":"text","data":"https://www.danasyariah.id/"}}',
                    'created_at' => '2021-06-23 09:53:14',
                    'updated_at' => '2021-06-23 09:53:25',
                ),
                20 =>
                array(
                    'id' => 21,
                    'slug' => 'redaksi',
                    'label' => 'Redaksi',
                    'value' => '{"dewanRedaksi":{"name":"dewanRedaksi","label":"Dewan Redaksi","type":"group","data":{"dewan1":{"name":"dewan1","label":"Dewan 1","type":"text","data":"KH. Aceng Zakaria KH."},"dewan2":{"name":"dewan2","label":"Dewan 2","type":"text","data":"Dr. Jeje Jaenudin, M.Ag."},"dewan3":{"name":"dewan3","label":"Dewan 3","type":"text","data":"Dr. Haris Muslim, MA."},"dewan4":{"name":"dewan4","label":"Dewan 4","type":"text","data":"Dr. Tiar Anwar Bactiar"},"dewan5":{"name":"dewan5","label":"Dewan 5","type":"text","data":"Pepen Irfan Fauzan, M.Hum"}}},"pimpinanRedaksi":{"name":"pimpinanRedaksi","label":"Pimpinan Redaksi","type":"group","data":{"pimpinan1":{"name":"pimpinan1","label":"Pimpinan 1","type":"text","data":"Muslim Nurdin, M.Hum"}}},"sekretarisRedaksi":{"name":"sekretarisRedaksi","label":"Sekretaris Redaksi","type":"group","data":{"sekretaris1":{"name":"sekretaris1","label":"Sekretaris 1","type":"text","data":"Taufik Ginanjar, S.Si."}}},"financialControl":{"name":"financialControl","label":"Financial Control","type":"group","data":{"financial1":{"name":"financial1","label":"Financial 1","type":"text","data":"Ismail Fajar Romdon, S.Pd."}}},"redaktur":{"name":"redaktur","label":"Redaktur","type":"group","data":{"redaktur1":{"name":"redaktur1","label":"Redaktur 1","type":"text","data":"Henry Lukmanul Hakim"},"redaktur2":{"name":"redaktur2","label":"Redaktur 2","type":"text","data":"Alvian Hamzah"},"redaktur3":{"name":"redaktur3","label":"Redaktur 3","type":"text","data":"Diki Mufid"}}},"editorPublisher":{"name":"editorPublisher","label":"Editor\/Publisher","type":"group","data":{"editor1":{"name":"editor1","label":"Editor\/Publisher 1","type":"text","data":"Daniyawan Haflah"},"editor2":{"name":"editor2","label":"Editor\/Publisher 2","type":"text","data":"Sofi Solihah"},"editor3":{"name":"editor3","label":"Editor\/Publisher 3","type":"text","data":"Fia Afifah"},"editor4":{"name":"editor4","label":"Editor\/Publisher 4","type":"text","data":"Ilmi Fadilah"}}},"reporter":{"name":"reporter","label":"Reporter","type":"group","data":{"reporter1":{"name":"reporter1","label":"Reporter 1","type":"text","data":"Asep Sofyan Nurdin"},"reporter2":{"name":"reporter2","label":"Reporter 2","type":"text","data":"Iman Nurjaman"},"reporter3":{"name":"reporter3","label":"Reporter 3","type":"text","data":"Rahma Anindia"},"reporter4":{"name":"reporter4","label":"Reporter 4","type":"text","data":"Dila Fadila F"},"reporter5":{"name":"reporter5","label":"Reporter 5","type":"text","data":"Yurkhi Khaerunnisa"}}},"fotographer":{"name":"fotographer","label":"Fotographer","type":"group","data":{"fotographer1":{"name":"fotographer1","label":"Fotographer 1","type":"text","data":"Agus Nur Putra"}}},"kontributor":{"name":"kontributor","label":"Kontributor","type":"group","data":{"kontributor1":{"name":"kontributor1","label":"Kontributor 1","type":"text","data":"Suhendi"},"kontributor2":{"name":"kontributor2","label":"Kontributor 2","type":"text","data":"Halim Majid"},"kontributor3":{"name":"kontributor3","label":"Kontributor 3","type":"text","data":"Muhammad Syifaurrahman"}}},"humas":{"name":"humas","label":"Humas","type":"group","data":{"humas1":{"name":"humas1","label":"Humas 1","type":"text","data":"Ricky Patria Yusei"}}},"websiteDevelopment":{"name":"websiteDevelopment","label":"Website Development","type":"group","data":{"websiteDevelopment1":{"name":"websiteDevelopment1","label":"Website Development 1","type":"text","data":"Muhammad Yahya Firdaus"},"websiteDevelopment2":{"name":"websiteDevelopment2","label":"Website Development 2","type":"text","data":"Hendi Santika"}}},"marketingKerjasama":{"name":"marketingKerjasama","label":"Marketing & Kerjasama","type":"group","data":{"marketingKerjasama1":{"name":"marketingKerjasama1","label":"Marketing & Kerjasama 1","type":"text","data":"Muhammad Asywanudin"},"marketingKerjasama2":{"name":"marketingKerjasama2","label":"Marketing & Kerjasama 2","type":"text","data":"Gicky Tamimi"}}}}',
                    'created_at' => '2021-06-23 09:53:14',
                    'updated_at' => '2021-06-23 09:53:25',
                ),
                21 =>
                array(
                    'id' => 22,
                    'slug' => 'kontak-kami',
                    'label' => 'Kontak Kami',
                    'value' => '{"alamatKantor":{"name":"alamatKantor","label":"Alamat Kantor","type":"group","data":{"bandung":{"name":"bandung","label":"Bandung","type":"text","data":"Jl. Perintis Kemerdekaan No 2 & 4 Bandung - Jawa Barat"}}},"teleponFax":{"name":"teleponFax","label":"Telepon\/Fax","type":"group","data":{"teleponFax1":{"name":"teleponFax1","label":"Telepon\/Fax 1","type":"group","data":{"telepon":{"name":"telepon","label":"Telepon","type":"text","data":"08123456789"},"fax":{"name":"fax","label":"Fax","type":"text","data":"12345"}}}}},"email":{"name":"email","label":"Email","type":"group","data":{"email1":{"name":"email1","label":"Email 1","type":"text","data":"johndoe@hotmail.com"}}}}',
                    'created_at' => '2021-06-23 09:53:14',
                    'updated_at' => '2021-06-23 09:53:25',
                ),
                22 =>
                array(
                    'id' => 23,
                    'slug' => 'disclaimer',
                    'label' => 'Disclaimer',
                    'value' => '{"paragraph1":{"name":"paragraph1","label":"Paragraph 1","type":"text","data":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam elit sapien, venenatis sit amet egestas a, placerat id diam. Duis rhoncus, felis eget iaculis fringilla, tellus libero faucibus dolor, nec ullamcorper urna tortor et felis. Maecenas varius cursus nisl, iaculis ultrices nisi auctor hendrerit. Morbi sit amet mauris placerat sem egestas blandit vel sit amet justo. Donec sodales vestibulum velit, vel vestibulum massa lobortis id. Etiam et fringilla dui. Duis tincidunt in ligula sit amet luctus. Ut rhoncus non quam sit amet tempus."},"paragraph2":{"name":"paragraph2","label":"Paragraph 2","type":"text","data":"Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec eget placerat purus. Ut vehicula varius est, at mollis arcu pellentesque vitae. In dictum quis neque ut ultricies. Curabitur fermentum justo eu interdum pulvinar. Etiam ut imperdiet lectus. Sed elementum vulputate leo eget congue. Curabitur sit amet magna vestibulum, pretium urna at, rutrum eros. Vestibulum sed tellus eu urna suscipit eleifend et ac magna."},"paragraph3":{"name":"paragraph3","label":"Paragraph 3","type":"text","data":"Morbi lorem ipsum, eleifend at molestie ut, posuere quis turpis. Aenean eget semper dolor. Cras sit amet interdum felis, vel convallis elit. Etiam sit amet lacus eu est vehicula dapibus eu id nunc. Praesent dignissim eros aliquam sem volutpat luctus. Aliquam ut odio convallis, ornare nisl sed, bibendum dui. Maecenas dignissim, nulla eget facilisis ultrices, urna nisi porttitor mi, ut suscipit nulla mi et lorem. Proin id facilisis odio. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec posuere varius orci, non ornare lacus efficitur eu. Aenean scelerisque finibus tincidunt."}}',
                    'created_at' => '2021-06-23 09:53:14',
                    'updated_at' => '2021-06-23 09:53:25',
                ),
                23 =>
                array(
                    'id' => 24,
                    'slug' => 'footer-banner',
                    'label' => 'Footer Banner',
                    'value' => '{"image":{"name":"image","label":"Banner Image","type":"image","data":"https://1.bp.blogspot.com/-YYVc5vrpXNQ/YAmHR2IThuI/AAAAAAAAGIk/6ks1vHnCCc0ng9-70mm4VV4tzgw9BnBAwCNcBGAsYHQ/s16000/donasi%2Bad1.jpg"},"url":{"name":"url","label":"Banner URL","type":"text","data":"https://kitasaudara.org/program/muktamar_xvi_persatuan_islam"}}',
                    'created_at' => '2021-06-23 09:53:14',
                    'updated_at' => '2021-06-23 09:53:25',
                ),
            ));

            \DB::commit();
        } catch (Exception $e) {
            \DB::rollBack();

            throw new Exception('Exception occur ' . $e);
        }
    }
}
