<?php

namespace Database\Seeders\Badaso\ManualGenerate;

use Illuminate\Database\Seeder;
use Uasoft\Badaso\Facades\Badaso;
use Uasoft\Badaso\Module\Post\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     *
     * @throws Exception
     */
    public function run()
    {
        \DB::beginTransaction();
        try {
            Category::query()->delete();

            Category::insert(array(
                0 =>
                array(
                    'id' => 1,
                    'parent_id' => NULL,
                    'title' => 'Berita',
                    'meta_title' => NULL,
                    'slug' => 'berita',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:37:52',
                    'updated_at' => '2021-07-19 04:37:52',
                ),
                1 =>
                array(
                    'id' => 2,
                    'parent_id' => NULL,
                    'title' => 'Otonom',
                    'meta_title' => NULL,
                    'slug' => 'otonom',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:38:01',
                    'updated_at' => '2021-07-19 04:38:01',
                ),
                2 =>
                array(
                    'id' => 3,
                    'parent_id' => NULL,
                    'title' => 'Kajian',
                    'meta_title' => NULL,
                    'slug' => 'kajian',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:38:08',
                    'updated_at' => '2021-07-19 04:38:08',
                ),
                3 =>
                array(
                    'id' => 4,
                    'parent_id' => NULL,
                    'title' => 'Kepesantrenan',
                    'meta_title' => NULL,
                    'slug' => 'kepesantrenan',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:38:16',
                    'updated_at' => '2021-07-19 04:38:16',
                ),
                4 =>
                array(
                    'id' => 5,
                    'parent_id' => 1,
                    'title' => 'Jammiyah',
                    'meta_title' => NULL,
                    'slug' => 'jammiyah',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:38:33',
                    'updated_at' => '2021-07-19 04:38:33',
                ),
                5 =>
                array(
                    'id' => 6,
                    'parent_id' => 1,
                    'title' => 'Nasional',
                    'meta_title' => NULL,
                    'slug' => 'nasional',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:38:43',
                    'updated_at' => '2021-07-19 04:38:43',
                ),
                6 =>
                array(
                    'id' => 7,
                    'parent_id' => 1,
                    'title' => 'Internasional',
                    'meta_title' => NULL,
                    'slug' => 'internasional',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:38:53',
                    'updated_at' => '2021-07-19 04:38:53',
                ),
                7 =>
                array(
                    'id' => 8,
                    'parent_id' => 2,
                    'title' => 'Persistri',
                    'meta_title' => NULL,
                    'slug' => 'persistri',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:39:13',
                    'updated_at' => '2021-07-19 04:39:13',
                ),
                8 =>
                array(
                    'id' => 9,
                    'parent_id' => 2,
                    'title' => 'Pemuda Persis',
                    'meta_title' => NULL,
                    'slug' => 'pemuda-persis',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:39:23',
                    'updated_at' => '2021-07-19 04:39:23',
                ),
                9 =>
                array(
                    'id' => 10,
                    'parent_id' => 2,
                    'title' => 'Pemudi Persis',
                    'meta_title' => NULL,
                    'slug' => 'pemudi-persis',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:39:31',
                    'updated_at' => '2021-07-19 04:39:31',
                ),
                10 =>
                array(
                    'id' => 11,
                    'parent_id' => 2,
                    'title' => 'Hima Persis',
                    'meta_title' => NULL,
                    'slug' => 'hima-persis',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:39:42',
                    'updated_at' => '2021-07-19 04:39:42',
                ),
                11 =>
                array(
                    'id' => 12,
                    'parent_id' => 2,
                    'title' => 'Himi Persis',
                    'meta_title' => NULL,
                    'slug' => 'himi-persis',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:39:54',
                    'updated_at' => '2021-07-19 04:39:54',
                ),
                12 =>
                array(
                    'id' => 13,
                    'parent_id' => 3,
                    'title' => 'Akidah',
                    'meta_title' => NULL,
                    'slug' => 'akidah',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:40:25',
                    'updated_at' => '2021-07-19 04:40:25',
                ),
                13 =>
                array(
                    'id' => 14,
                    'parent_id' => 3,
                    'title' => 'Dakwah',
                    'meta_title' => NULL,
                    'slug' => 'dakwah',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:40:35',
                    'updated_at' => '2021-07-19 04:40:35',
                ),
                14 =>
                array(
                    'id' => 15,
                    'parent_id' => 3,
                    'title' => 'Muamalah',
                    'meta_title' => NULL,
                    'slug' => 'muamalah',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:40:46',
                    'updated_at' => '2021-07-19 04:40:46',
                ),
                15 =>
                array(
                    'id' => 16,
                    'parent_id' => 3,
                    'title' => 'Ibadah',
                    'meta_title' => NULL,
                    'slug' => 'ibadah',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:40:54',
                    'updated_at' => '2021-07-19 04:40:54',
                ),
                16 =>
                array(
                    'id' => 17,
                    'parent_id' => 3,
                    'title' => 'Siyasah',
                    'meta_title' => NULL,
                    'slug' => 'siyasah',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:41:06',
                    'updated_at' => '2021-07-19 04:41:06',
                ),
                17 =>
                array(
                    'id' => 18,
                    'parent_id' => 3,
                    'title' => 'Hisab Rukyat',
                    'meta_title' => NULL,
                    'slug' => 'hisab-rukyat',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:41:17',
                    'updated_at' => '2021-07-19 04:41:17',
                ),
                18 =>
                array(
                    'id' => 19,
                    'parent_id' => 3,
                    'title' => 'Tarbiyyah',
                    'meta_title' => NULL,
                    'slug' => 'tarbiyyah',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:41:40',
                    'updated_at' => '2021-07-19 04:41:40',
                ),
                19 =>
                array(
                    'id' => 20,
                    'parent_id' => 3,
                    'title' => 'Khazanah',
                    'meta_title' => NULL,
                    'slug' => 'waketum',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:41:51',
                    'updated_at' => '2021-07-19 04:41:51',
                ),
                20 =>
                array(
                    'id' => 21,
                    'parent_id' => 1,
                    'title' => 'Info Buku',
                    'meta_title' => NULL,
                    'slug' => 'info-buku',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:42:01',
                    'updated_at' => '2021-07-19 04:42:01',
                ),
                21 =>
                array(
                    'id' => 22,
                    'parent_id' => NULL,
                    'title' => 'Muktamar',
                    'meta_title' => NULL,
                    'slug' => 'muktamar',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:42:17',
                    'updated_at' => '2021-07-19 04:42:17',
                ),
                22 =>
                array(
                    'id' => 23,
                    'parent_id' => 22,
                    'title' => 'Road To Muktamar 16',
                    'meta_title' => NULL,
                    'slug' => 'road-to-muktamar-16',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:42:31',
                    'updated_at' => '2021-07-19 04:42:31',
                ),
                23 =>
                array(
                    'id' => 24,
                    'parent_id' => NULL,
                    'title' => 'Majalah Risalah',
                    'meta_title' => NULL,
                    'slug' => 'risalah',
                    'content' => NULL,
                    'created_at' => '2021-07-19 04:43:07',
                    'updated_at' => '2021-07-19 04:43:07',
                ),
            ));

            \DB::commit();
        } catch (Exception $e) {
            \DB::rollBack();

            throw new Exception('Exception occur ' . $e);
        }
    }
}
