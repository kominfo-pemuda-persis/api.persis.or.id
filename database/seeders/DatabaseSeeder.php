<?php

namespace Database\Seeders;

use Database\Seeders\Badaso\BadasoSeeder;
use Database\Seeders\Badaso\Post\BadasoPostModuleSeeder;
use Database\Seeders\Badaso\Content\BadasoContentModuleSeeder;
use Database\Seeders\Badaso\CRUD\BadasoDeploymentOrchestratorSeeder;
use Database\Seeders\Badaso\ManualGenerate\BadasoManualGenerateSeeder;
use Database\Seeders\Badaso\ManualGenerate\ContentTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BadasoSeeder::class);
        $this->call(BadasoPostModuleSeeder::class);
        $this->call(BadasoContentModuleSeeder::class);
        $this->call(BadasoDeploymentOrchestratorSeeder::class);
        $this->call(BadasoManualGenerateSeeder::class);
    }
}
