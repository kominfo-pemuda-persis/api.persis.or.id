<?php

return [
    'post_url_prefix' => env('MIX_POST_URL_PREFIX', ''),
    'fronted_url' => env('MIX_FRONTEND_URL', 'https://persis.or.id')
];
